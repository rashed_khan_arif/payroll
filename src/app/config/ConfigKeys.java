package app.config;

/**
 * Created by arifk on 25.8.17.
 */
public class ConfigKeys {
    public static final int DB = 1;
    public static final int FILE = 2;
    public static final int CLOUD = 3;
    public static final String FILE_ROUTE = "c:/hr/";
    public static final String COMPANY_CODE = "TL";

}
