package app.model;

/**
 * Created by arifk on 24.8.17.
 */
public class EmployeeCategory {
    private int catId;
    private String catName;
    private String catDesc;

    public int getCat_id() {
        return catId;
    }

    public void setCat_id(int cat_id) {
        this.catId = cat_id;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatDesc() {
        return catDesc;
    }

    public void setCatDesc(String catDesc) {
        this.catDesc = catDesc;
    }

    @Override
    public String toString() {
        return catName;
    }
}
