package app.model;

/**
 * Created by arifk on 22.8.17.
 */
public class LoginData {
    private String empCode;
    private String passWord;

    public String getUserId() {
        return empCode;
    }

    public void setUserId(String userId) {
        this.empCode = userId;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
}
