package app.model;

import java.util.Date;

/**
 * Created by arifk on 8.11.17.
 */
public class ProjectDetails {
    private int projectDetailsId;
    private int empId;
    private int projectId;
    private Employee employee;
    private Project project;
    private Date joinDate;

    public int getProjectDetailsId() {
        return projectDetailsId;
    }

    public void setProjectDetailsId(int projectDetailsId) {
        this.projectDetailsId = projectDetailsId;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
