package app.modules.Employee;

import app.model.*;
import app.modules.Common.BaseView;


import java.util.List;

/**
 * Created by arifk on 23.8.17.
 */
public interface EmployeeContract {
    interface EmployeeView extends BaseView {

        void setEmployeeData(List<Employee> employees);

        void setEmployeeDetails(Employee employeeDetails);
    }

    interface AddEmployeeView extends BaseView {

    }

    interface EmployeePresenter {
        void getEmployeeData();

        List<Employee> getEmpByJobId(int id);

        List<Employee> getEmpByDeptId(int Id);

        List<Employee> getEmpByCatId(int Id);

        List<Employee> getEmpByTypeId(int Id);

        void getEmployeeDetails(int empId);

        String getCountryName(int cId);
    }

    interface AddEmployeePresenter {
        Employee getEmployee(int id);

        List<Department> getDepartment();

        List<EmployeeCategory> getCategory();

        List<EmployeeType> getType();

        List<Country> getCountry();

        List<Job> getJobByDeptId(int deptId);

        boolean addNewEmployee(Employee employee);
    }
}
