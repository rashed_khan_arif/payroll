package app.modules.Salary;

import app.model.Employee;
import app.model.Payroll;
import app.model.Salary;
import app.modules.Common.BaseView;
import com.sun.istack.internal.Nullable;

import java.util.List;

/**
 * Created by arifk on 11.12.17.
 */
public interface SalaryContract {
    interface SalaryPresenter {

        int insertSalary(Salary salary);

        int updateSalary(Salary salary);

        int insertPayroll(Payroll payroll);

        int updatePayroll(Payroll payroll);

        void getActiveEmployee();

        Salary getSalaryByEmpId(int empId, int year);

        void getSalaryList();

        void getSalaryList(int empId);

        void getPayList();

        void getPayList(@Nullable Integer empId, @Nullable Integer year, @Nullable Integer month);

        Payroll getPayrollByEmpId(int empId, int year, int month);
    }

    interface salaryView extends BaseView {
        void setEmployeeToView(List<Employee> employees);

        void setPayListToView(List<Payroll> payList);

        void setSalariesToView(List<Salary> salaries);
    }
}
