package app.modules.Auth;

import app.config.DataSource;
import app.core.UserSession;
import app.data.DAOFactory;
import app.model.LoginData;
import app.model.User;
import app.modules.Common.Base;

/**
 * Created by arifk on 21.8.17.
 */
public class AuthPresenter extends Base implements AuthContract.AuthPresenter, AuthContract.SessionPresenter {
    private AuthContract.AuthView view;
    private DAOFactory daoFactory = DataSource.getDataSource();

    public AuthPresenter(AuthContract.AuthView authView) {
        view = authView;
    }

    @Override
    public void checkLoginInfo(LoginData login) {
//        authRepo.checkLoginInfo(login, new ResponseListener<User>() {
//            @Override
//            public void success(User success) {
//                view.isAuth(success != null);
//            }
//
//            @Override
//            public void error(String error) {
//                view.isAuth(false);
//            }
//        });
        User user = daoFactory.getAuthDAO().checkLoginInfo(login);
        if (user != null) {
            user.setEmployee(daoFactory.getEmployeeDao().getEmployee(user.getEmpId()));
            user.setRole(daoFactory.getUserRoleDAO().getUserRole(user.getRoleId()));
            view.isAuth(user.isActive(), user);
        } else
            view.isAuth(false, null);
    }

    @Override
    public int setCurrentSession(UserSession session) {
        return daoFactory.getAuthDAO().setUserSession(session);
    }

    @Override
    public UserSession getCurrentUserSession(int userId) {
        UserSession session = daoFactory.getAuthDAO().getCurrentSession(userId);
        session.setUser(daoFactory.getUserDAO().getUser(session.getUserId()));
        return session;
    }

    @Override
    public boolean updateCurrentSession(UserSession session) {
        return false;
    }

    @Override
    public boolean checkLogin() {
        return false;
    }
}
