package app.modules.Attendence;

import app.config.DataSource;
import app.data.DAOFactory;
import app.model.Employee;
import app.model.Project;
import app.model.TimeSheet;
import com.sun.istack.internal.Nullable;

import java.sql.Time;
import java.util.*;

/**
 * Created by arifk on 21.8.17.
 */
public class AttendancePresenter implements AttendanceContract.Presenter {
    private AttendanceContract.View view;

    DAOFactory dao;

    public AttendancePresenter(AttendanceContract.View view) {
        this.view = view;
        dao = DataSource.getDataSource();
    }

    @Override
    public void addAttendance(TimeSheet timeSheet) {
        int attId = timeSheet.getCheckOut() != null ? dao.getAttendanceDAO().updateAttendance(timeSheet) :
                dao.getAttendanceDAO().addAttendance(timeSheet);
        if (attId != 0)
            view.success("Attendance taken");
        else
            view.error("Failed");
    }

    @Override
    public void getEmployees() {
        List<Employee> employees = dao.getEmployeeDao().getEmployeeList();
        view.setEmployeeListToView(employees);
    }

    @Override
    public TimeSheet getAttendance(int empId, Date date) {
        return dao.getAttendanceDAO().getAttendance(empId, date);
    }

    @Override
    public void searchAttendanceReport(@Nullable Integer empId, @Nullable Integer year, @Nullable Integer month, @Nullable Integer day) {
        List<TimeSheet> timeSheets = dao.getAttendanceDAO().searchAttendance(empId == 0 ? null : empId, year, month, day == 0 ? null : day);
        for (TimeSheet sheet : timeSheets) {
            Project project = dao.getProjectDAO().getProject(sheet.getProjectId());
            Employee emp = dao.getEmployeeDao().getEmployee(sheet.getEmpId());
            sheet.setProject(project);
            sheet.setEmployee(emp);
        }
        view.setAttendanceReportToView(timeSheets);
    }

    @Override
    public void getProjects() {
        List<Project> projects = dao.getProjectDAO().getProjectList();
        view.setProjectsToView(projects);
    }

    @Override
    public void getLastAttendanceList() {
        List<TimeSheet> timeSheets = dao.getAttendanceDAO().getCurrentAttendanceList();
        timeSheets.forEach(timeSheet -> {
            timeSheet.setEmployee(dao.getEmployeeDao().getEmployee(timeSheet.getEmpId()));
            timeSheet.setProject(dao.getProjectDAO().getProject(timeSheet.getProjectId()));
        });
        view.setAttendanceListToView(timeSheets);
    }

    @Override
    public void getAttendanceListByMonth(int year, int month) {
        List<Employee> employees = dao.getEmployeeDao().getEmployeeList(1);
        for (Employee emp : employees) {
            List<TimeSheet> timeSheets = dao.getAttendanceDAO().searchAttendance(emp.getEmpId(), year, month, null);
            emp.setAttendance(timeSheets);
        }
        view.setAttendanceByMonthListToView(employees);
    }
}
