package app.modules.Setting;

import app.model.*;
import app.modules.Common.BaseView;

import java.util.List;

/**
 * Created by arifk on 20.9.17.
 */
public interface SettingContract {
    interface SettingView extends BaseView {
        void setDepartmentDataToView(List<Department> departments);

        void setJobsData(List<Job> jobs);

        void setTypes(List<EmployeeType> employeeTypes);

        void setCategoriesData(List<EmployeeCategory> employeeCategories);

        void setUserList(List<User> userList);

        void setUserRolesToView(List<UserRole> roleList);

        void setEmployeeListToCombo(List<Employee> employeeList);

    }

    interface AdminPresenter {
        void getDepartments();

        void getJobs();

        void getEmployeeList();

        void getEmployeeTypes();

        void getEmployeeCategories();

        void getUserList();

        User getUser(int userId);

        Employee checkEmpId(String code);

        boolean createUser(User user);

        int createType(EmployeeType type);

        int createCategory(EmployeeCategory category);
        int createJob(Job job);

        int createDepartment(Department department);

        void getUserRoles();

        boolean checkUserExits(int empId);

    }
}
