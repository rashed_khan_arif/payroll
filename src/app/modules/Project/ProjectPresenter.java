package app.modules.Project;

import app.config.DataSource;
import app.data.DAOFactory;
import app.data.db.DBFactory;
import app.data.db.dao_impl.DBProjectDAOImpl;
import app.model.Employee;
import app.model.Project;
import app.model.ProjectDetails;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by arifk on 21.8.17.
 */
public class ProjectPresenter implements ProjectContract.ProjectPresenter {
    ProjectContract.ProjectView view;
    DAOFactory daoFactory;

    public ProjectPresenter(ProjectContract.ProjectView view) {
        this.view = view;
        daoFactory = DataSource.getDataSource();
    }

    @Override
    public int addProject(Project project) {
        return daoFactory.getProjectDAO().addProject(project);

    }

    @Override
    public void getManagerList() {
        view.setMangerListToView(daoFactory.getEmployeeDao().getEmployeeList(1));
    }

    @Override
    public void getDepartmentList() {
        view.setDepartmentListToView(daoFactory.getDepartmentDAO().getDepartmentList());

    }

    @Override
    public void getProjectList() {
        List<Project> projects = daoFactory.getProjectDAO().getProjectList();
        for (Project nProject : projects) {
            Employee manager = daoFactory.getEmployeeDao().getEmployee(nProject.getManagerId());
            nProject.setManager(manager);
            nProject.setDepartment(daoFactory.getDepartmentDAO().getDepartment(nProject.getDeptId()));
        }
        view.setProjectListToView(projects);
    }

    @Override
    public int assignProject(ProjectDetails projectDetails) {
        return daoFactory.getProjectDAO().assignProject(projectDetails);
    }

    @Override
    public void getAssignedMembersList(int projectId) {
        List<ProjectDetails> details = daoFactory.getProjectDAO().getAssignedProjectMemberList(projectId);
        for (ProjectDetails pd : details) {
            pd.setEmployee(daoFactory.getEmployeeDao().getEmployee(pd.getEmpId()));
            pd.setProject(daoFactory.getProjectDAO().getProject(pd.getProjectId()));
        }
        view.setAssignedMembersToView(details);
    }
}
