package app.data.cloud;

import app.data.DAOFactory;
import app.data.dao.*;

/**
 * Created by arifk on 25.8.17.
 */

public class CloudFactory extends DAOFactory {

    @Override
    public EmployeeDAO getEmployeeDao() {
        return null;
    }

    @Override
    public DepartmentDAO getDepartmentDAO() {
        return null;
    }

    @Override
    public EmployeeAddressDAO getEmployeeAddressDAO() {
        return null;
    }

    @Override
    public EmployeeCategoryDAO getEmployeeCategoryDAO() {
        return null;
    }

    @Override
    public EmployeeTypeDAO getEmployeeTypeDAO() {
        return null;
    }

    @Override
    public JobDAO getJobDAO() {
        return null;
    }

    @Override
    public ProjectDAO getProjectDAO() {
        return null;
    }

    @Override
    public AttendanceDAO getAttendanceDAO() {
        return null;
    }

    @Override
    public UserDAO getUserDAO() {
        return null;
    }

    @Override
    public UserRoleDAO getUserRoleDAO() {
        return null;
    }

    @Override
    public SalaryDAO getSalaryDAO() {
        return null;
    }

    @Override
    public AuthDAO getAuthDAO() {
        return null;
    }
}
