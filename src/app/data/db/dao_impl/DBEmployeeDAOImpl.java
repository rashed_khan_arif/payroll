package app.data.db.dao_impl;

import app.core.DB;
import app.core.ResultParser;
import app.data.dao.EmployeeDAO;
import app.model.Country;
import app.model.Employee;
import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arifk on 25.8.17.
 */
public class DBEmployeeDAOImpl implements EmployeeDAO {
    private Connection con = null;

    public DBEmployeeDAOImpl() {
        con = DB.getConnection();
    }

    @Override
    public List<Country> getCountries() {
        PreparedStatement ps = null;
        Country country;
        List<Country> countries = new ArrayList<>();
        try {
            String query = "SELECT * FROM countries";
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                country = ResultParser.parser(rs, Country.class);
                countries.add(country);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return countries;
    }

    @Override
    public Employee getEmployee(int empId) {
        PreparedStatement ps = null;
        Employee employee = null;
        try {
            String query = "SELECT * FROM employees WHERE empId =" + empId;
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                employee = ResultParser.parser(rs, Employee.class);
            }
            return employee;

        } catch (SQLException e) {
            return null;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    @Override
    public Employee getEmployee(String code) {
        PreparedStatement ps = null;
        Employee employee = null;
        try {
            String query = "SELECT * FROM employees WHERE empCode ='" + code + "'";
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                employee = ResultParser.parser(rs, Employee.class);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return employee;
    }

    @Override
    public List<Employee> getEmployeeList() {
        PreparedStatement ps = null;
        List<Employee> employees = new ArrayList<>();
        try {
            String query = "SELECT * FROM employees ORDER by empId DESC ";
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            Employee employee = null;
            while (rs.next()) {
                employee = ResultParser.parser(rs, Employee.class);
                employees.add(employee);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }

        return employees;
    }

    @Override
    public List<Employee> getEmployeeList(int active) {
        PreparedStatement ps = null;
        List<Employee> employees = new ArrayList<>();
        try {
            String query = "SELECT * FROM employees where active = " + active + " order by empId DESC ";
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            Employee employee = null;
            while (rs.next()) {
                employee = ResultParser.parser(rs, Employee.class);
                employees.add(employee);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }

        return employees;
    }


    @Override
    public int addNewEmployee(Employee employee) {
        PreparedStatement ps = null;
        try {
            String query = "INSERT INTO employees (firstName,lastName,catId,deptId,jobId,typeId,email," +
                    "cellPhone,officePhone,dob,active,ssn,empCode,gender,image) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = (PreparedStatement) con.prepareStatement(query);
            ps.setString(1, employee.getFirstName());
            ps.setString(2, employee.getLastName());
            ps.setInt(3, employee.getCatId());
            ps.setInt(4, employee.getDepartmentId());
            ps.setInt(5, employee.getJobId());
            ps.setInt(6, employee.getTypeId());
            ps.setString(7, employee.getEmail());
            ps.setInt(8, employee.getCellPhone());
            ps.setInt(9, employee.getOfficePhone());
            ps.setString(10, employee.getDob().toString());
            ps.setInt(11, employee.getActive());
            ps.setString(12, employee.getSsn());
            ps.setString(13, employee.getEmpCode());
            ps.setInt(14, employee.getGender());
            ps.setString(15, employee.getImage());
            ps.executeUpdate();
            int id = 0;
            ResultSet set = ps.getGeneratedKeys();

            while (set.next()) {
                id = (int) set.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return 0;
    }

    @Override
    public boolean updateEmployee(Employee employee) {
        PreparedStatement ps = null;
        try {
            String query = "UPDATE employees SET firstName=?,lastName=?,catId=?,deptId=?,jobId=?,typeId=?,email=?," +
                    "cellPhone=?,officePhone=?,dob=?,active=?,ssn=?,gender=?,image=? WHERE empId=?";
            ps = (PreparedStatement) con.prepareStatement(query);
            ps.setString(1, employee.getFirstName());
            ps.setString(2, employee.getLastName());
            ps.setInt(3, employee.getCatId());
            ps.setInt(4, employee.getDepartmentId());
            ps.setInt(5, employee.getJobId());
            ps.setInt(6, employee.getTypeId());
            ps.setString(7, employee.getEmail());
            ps.setInt(8, employee.getCellPhone());
            ps.setInt(9, employee.getOfficePhone());
            ps.setString(10, employee.getDob().toString());
            ps.setInt(11, employee.getActive());
            ps.setString(12, employee.getSsn());
            ps.setInt(13, employee.getGender());
            ps.setString(14, employee.getImage());
            ps.setInt(15, employee.getEmpId());
            int i = ps.executeUpdate();
            return i != 0;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        con.close();
    }
}
