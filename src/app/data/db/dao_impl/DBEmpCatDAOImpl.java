package app.data.db.dao_impl;

import app.core.DB;
import app.core.ResultParser;
import app.data.dao.EmployeeCategoryDAO;
import app.model.EmployeeCategory;
import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arifk on 20.9.17.
 */
public class DBEmpCatDAOImpl implements EmployeeCategoryDAO {
    private Connection con;

    public DBEmpCatDAOImpl() {
        con = DB.getConnection();
    }

    @Override
    public int addEmpCat(EmployeeCategory category) {
        PreparedStatement ps = null;
        try {
            String query = "INSERT INTO emp_category (catId,catName,catDesc) VALUES (?,?,?)";
            ps = (PreparedStatement) con.prepareStatement(query);
            ps.setInt(1, category.getCat_id());
            ps.setString(2, category.getCatName());
            ps.setString(3, category.getCatDesc());
            ps.executeUpdate();
            int id = 0;
            ResultSet set = ps.getGeneratedKeys();
            while (set.next()) {
                id = (int) set.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return 0;
    }

    @Override
    public boolean updateEmpCat(EmployeeCategory category) {
        return false;
    }

    @Override
    public EmployeeCategory getCat(int catId) {
        PreparedStatement ps = null;
        EmployeeCategory cat = null;
        try {
            String query = "SELECT * FROM emp_category WHERE catId=" + catId;
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                cat = ResultParser.parser(rs, EmployeeCategory.class);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return cat;
    }

    @Override
    public List<EmployeeCategory> getCatList() {
        PreparedStatement ps = null;
        List<EmployeeCategory> employeeCategories = new ArrayList<>();
        try {
            String query = "SELECT * FROM emp_category ";
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            EmployeeCategory employeeCategory = null;
            while (rs.next()) {
                employeeCategory = ResultParser.parser(rs, EmployeeCategory.class);
                employeeCategories.add(employeeCategory);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return employeeCategories;
    }
}
