package app.data.db.dao_impl;

import app.core.DB;
import app.core.ResultParser;
import app.core.UserSession;
import app.data.dao.AuthDAO;
import app.model.*;
import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by arifk on 25.8.17.
 */
public class DBAuthDAOImpl implements AuthDAO {
    private Connection con;

    public DBAuthDAOImpl() {
        this.con = DB.getConnection();
    }

    @Override
    public void createOrUpdateUser(User user) {

    }


    @Override
    public User checkLoginInfo(LoginData loginData) {
        PreparedStatement ps = null;
        try {
            String query = "select u.* from users u JOIN " +
                    "employees e ON u.empId=e.empId " +
                    "JOIN role r ON u.roleId=r.roleId " +
                    "where e.empCode='" + loginData.getUserId() + "' " +
                    "and u.password = Md5('" + loginData.getPassWord() + "')" +
                    "and e.active=1 and u.active=1";
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            User user = new User();
            while (rs.next()) {
                user.setEmpId(rs.getInt("empId"));
                user.setRoleId(rs.getInt("roleId"));
                user.setUserId(rs.getInt("userId"));
                user.setActive(rs.getInt("active"));
            }
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public UserSession getCurrentSession(int userId) {
        PreparedStatement ps = null;
        UserSession session = null;
        try {
            String query = "SELECT * FROM temp_session WHERE userId ='" + userId + "'";
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                session = ResultParser.parser(rs, UserSession.class);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return session;
    }

    @Override
    public int setUserSession(UserSession userSession) {
        PreparedStatement ps = null;
        try {
            java.sql.Date login = new java.sql.Date(userSession.getStartLogin().getTime());
            java.sql.Date logout = userSession.getLogOutTime() == null ? null : new java.sql.Date(userSession.getLogOutTime().getTime());
            String query = "INSERT INTO temp_session (userSessionId,userId,startLogin,isLogin,logOutTime) VALUES (?,?,?,?,?)";
            ps = (PreparedStatement) con.prepareStatement(query);
            ps.setInt(1, userSession.getUserSessionId());
            ps.setInt(2, userSession.getUserId());
            ps.setDate(3, login);
            ps.setInt(4, userSession.getIsLogin());
            ps.setDate(5, logout);

            ps.executeUpdate();
            int id = 0;
            ResultSet set = ps.getGeneratedKeys();
            while (set.next()) {
                id = (int) set.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            return 0;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public int updateUserSession(UserSession userSession) {
        PreparedStatement ps = null;
        try {
            java.sql.Date login = new java.sql.Date(userSession.getStartLogin().getTime());
            java.sql.Date logout = new java.sql.Date(userSession.getLogOutTime().getTime());
            String query = "UPDATE temp_session  SET userSessionId=?,userId=?,startLogin=?,isLogin=?,logOutTime=? WHERE userSessionId=" + userSession.getUserSessionId();
            ps = (PreparedStatement) con.prepareStatement(query);
            ps.setInt(1, userSession.getUserSessionId());
            ps.setInt(2, userSession.getUserId());
            ps.setDate(3, login);
            ps.setInt(4, userSession.getIsLogin());
            ps.setDate(5, logout);
            ps.executeUpdate();
            int i = ps.executeUpdate();
            return i;
        } catch (SQLException e) {
            return 0;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        con.close();
    }
}
