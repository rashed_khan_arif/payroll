package app.data.db.dao_impl;

import app.core.DB;
import app.core.ResultParser;
import app.data.dao.EmployeeAddressDAO;
import app.model.Address;
import app.model.Country;
import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by arifk on 20.9.17.
 */
public class DBEmpAddressDAOImpl implements EmployeeAddressDAO {
    private Connection con;

    public DBEmpAddressDAOImpl() {
        con = DB.getConnection();
    }

    @Override
    public boolean addAddress(Address address) {
        PreparedStatement ps = null;
        try {
            String query = "INSERT INTO emp_address (address1,address2,city,state,zip,countryId,empId) VALUES (?,?,?,?,?,?,?)";
            ps = (PreparedStatement) con.prepareStatement(query);
            ps.setString(1, address.getAddress1());
            ps.setString(2, address.getAddress2());
            ps.setString(3, address.getCity());
            ps.setString(4, address.getState());
            ps.setInt(5, address.getZip());
            ps.setInt(6, address.getCountryId());
            ps.setInt(7, address.getEmp_id());
            int i = ps.executeUpdate();
            return i != 0;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public boolean updateAddress(Address address) {
        PreparedStatement ps = null;
        try {
            String query = "UPDATE emp_address SET address1=?,address2=?,city=?,state=?,zip=?,countryId=?,empId=? WHERE addressId=?";
            ps = (PreparedStatement) con.prepareStatement(query);
            ps.setString(1, address.getAddress1());
            ps.setString(2, address.getAddress2());
            ps.setString(3, address.getCity());
            ps.setString(4, address.getState());
            ps.setInt(5, address.getZip());
            ps.setInt(6, address.getCountryId());
            ps.setInt(7, address.getEmp_id());
            ps.setInt(8, address.getAddressId());
            int i = ps.executeUpdate();
            return i != 0;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public Address getAddress(int addressId) {
        PreparedStatement ps = null;
        Address address = null;
        try {
            String query = "SELECT * FROM emp_address WHERE addressId=" + addressId;
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                address = ResultParser.parser(rs, Address.class);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return address;
    }

    @Override
    public Address getAddressByEmployee(int empId) {
        PreparedStatement ps = null;
        try {
            String query = "SELECT * FROM emp_address WHERE empId=" + empId;
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            Address address = null;
            while (rs.next()) {
                address = ResultParser.parser(rs, Address.class);
                if (address.getEmp_id() == empId) {
                    return address;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public String getCountryNameByCountryId(int cId) {
        PreparedStatement ps = null;
        try {
            String query = "SELECT * FROM countries WHERE id=" + cId;
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            Country country = null;
            while (rs.next()) {
                country = ResultParser.parser(rs, Country.class);
                if (country.getId() == cId) {
                    return country.getCon_name();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
