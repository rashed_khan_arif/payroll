package app.data.db.dao_impl;

import app.core.DB;
import app.core.ResultParser;
import app.data.dao.AttendanceDAO;
import app.model.Department;
import app.model.Employee;
import app.model.TimeSheet;
import com.mysql.jdbc.PreparedStatement;
import com.sun.istack.internal.Nullable;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.sun.jmx.snmp.EnumRowStatus.active;

/**
 * Created by arifk on 23.10.17.
 */
public class DBAttendanceDAOImpl implements AttendanceDAO {
    private Connection con;

    public DBAttendanceDAOImpl() {
        this.con = DB.getConnection();
    }

    @Override
    public TimeSheet getAttendance(java.util.Date searchDate) {
        java.sql.Date sqlDate = new java.sql.Date(searchDate.getTime());
        PreparedStatement ps = null;
        TimeSheet timeSheet = null;
        try {
            String query = "SELECT * FROM timesheet WHERE attendDate=" + sqlDate;
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                timeSheet = ResultParser.parser(rs, TimeSheet.class);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return timeSheet;
    }

    @Override
    public TimeSheet getAttendance(int empId) {
        PreparedStatement ps = null;
        TimeSheet timeSheet = null;
        try {
            String query = "SELECT * FROM timesheet WHERE empId=" + empId;
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                timeSheet = ResultParser.parser(rs, TimeSheet.class);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return timeSheet;
    }

    @Override
    public List<TimeSheet> searchAttendance(@Nullable Integer empId, @Nullable Integer year, @Nullable Integer month, @Nullable Integer day) {
        PreparedStatement ps = null;
        List<TimeSheet> timeSheets = new ArrayList<>();
        try {
            String query = "SELECT * FROM timesheet WHERE ";


            if (empId != null) {

                query = query.concat("empId=" + empId);

            }
            if (year != null) {
                if (empId != null) {
                    query = query.concat(" AND YEAR (attendDate) =" + year);
                } else
                    query = query.concat(" YEAR (attendDate)=" + year);

            }
            if (month != null) {
                if (empId != null || year != null) {
                    query = query.concat(" AND MONTH (attendDate)=" + month);
                } else {
                    query = query.concat(" MONTH (attendDate)=" + month);
                }
            }
            if (day != null) {
                if (empId != null || year != null || month != null) {
                    query = query.concat(" AND DAY (attendDate) =" + day);
                } else {
                    query = query.concat(" DAY (attendDate) =" + day);
                }
            }


            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            TimeSheet timeSheet = null;
            while (rs.next()) {
                timeSheet = ResultParser.parser(rs, TimeSheet.class);
                timeSheets.add(timeSheet);
            }
        } catch (SQLException e) {
            return null;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return timeSheets;
    }

    @Override
    public List<TimeSheet> getAttendanceListByMonth(int year, int month) {
        PreparedStatement ps = null;
        List<TimeSheet> timeSheets = new ArrayList<>();
        try {
            String query = "SELECT * FROM timesheet WHERE  YEAR (attendDate)=" + year + " AND MONTH (attendDate) =" + month;
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            TimeSheet timeSheet = null;
            while (rs.next()) {
                timeSheet = ResultParser.parser(rs, TimeSheet.class);
                timeSheets.add(timeSheet);
            }
        } catch (SQLException e) {
            return null;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return timeSheets;
    }


    @Override
    public TimeSheet getAttendance(int empId, java.util.Date date) {
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        PreparedStatement ps = null;
        TimeSheet timeSheet = null;
        try {
            String query = "SELECT * FROM timesheet WHERE empId=" + empId + " AND DAY (attendDate) = DAY (CURRENT_DATE())" + " AND MONTH (attendDate) = MONTH (CURRENT_DATE())" + " AND YEAR (attendDate)= YEAR (CURRENT_DATE())";
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                timeSheet = ResultParser.parser(rs, TimeSheet.class);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return timeSheet;
    }

    @Override
    public int addAttendance(TimeSheet timeSheet) {
        PreparedStatement ps = null;
        try {
            java.sql.Date sqlDate = new java.sql.Date(timeSheet.getAttendDate().getTime());

            String query = "INSERT INTO timesheet (empId,projectId,attendDate,checkIn,checkOut,workDesc) VALUES (?,?,?,?,?,?)";
            ps = (PreparedStatement) con.prepareStatement(query);
            ps.setInt(1, timeSheet.getEmpId());
            ps.setInt(2, timeSheet.getProjectId());
            ps.setDate(3, sqlDate);
            ps.setTime(4, timeSheet.getCheckIn());
            ps.setTime(5, timeSheet.getCheckOut());
            ps.setString(6, timeSheet.getWorkDesc());
            ps.executeUpdate();
            int id = 0;
            ResultSet set = ps.getGeneratedKeys();
            while (set.next()) {
                id = (int) set.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            return 0;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    @Override
    public int updateAttendance(TimeSheet timeSheet) {
        PreparedStatement ps = null;
        try {
            java.sql.Date sqlDate = new java.sql.Date(timeSheet.getAttendDate().getTime());

            String query = "Update timesheet SET timesheetId=?,empId=?, projectId=?, attendDate=?, checkIn=?,checkOut=?, workDesc=? WHERE timesheetId=" + timeSheet.getTimesheetId();
            ps = (PreparedStatement) con.prepareStatement(query);
            ps.setInt(1, timeSheet.getTimesheetId());
            ps.setInt(2, timeSheet.getEmpId());
            ps.setInt(3, timeSheet.getProjectId());
            ps.setDate(4, sqlDate);
            ps.setTime(5, timeSheet.getCheckIn());
            ps.setTime(6, timeSheet.getCheckOut());
            ps.setString(7, timeSheet.getWorkDesc());
            return ps.executeUpdate();
        } catch (SQLException e) {
            return 0;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    @Override
    public List<TimeSheet> getCurrentAttendanceList() {
        PreparedStatement ps = null;
        List<TimeSheet> timeSheets = new ArrayList<>();
        try {
            String query = "SELECT * FROM timesheet WHERE DAY (attendDate)= DAY (CURRENT_DATE()) AND MONTH (attendDate)=MONTH (CURRENT_DATE()) AND YEAR(attendDate) = YEAR(CURRENT_DATE())";
            ps = (PreparedStatement) con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            TimeSheet timeSheet = null;
            while (rs.next()) {
                timeSheet = ResultParser.parser(rs, TimeSheet.class);
                timeSheets.add(timeSheet);
            }
        } catch (SQLException e) {
            return null;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        return timeSheets;
    }

    @Override
    public boolean deleteAttendance(int timeSheetId) {
        return false;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        con.close();
    }
}
