package app.data.db;

import app.core.DB;
import app.data.DAOFactory;
import app.data.dao.*;
import app.data.db.dao_impl.*;

import java.sql.Connection;

/**
 * Created by arifk on 25.8.17.
 */
public class DBFactory extends DAOFactory {

    @Override
    public EmployeeDAO getEmployeeDao() {
        return new DBEmployeeDAOImpl();
    }

    @Override
    public DepartmentDAO getDepartmentDAO() {
        return new DBDeptImpl();
    }

    @Override
    public EmployeeAddressDAO getEmployeeAddressDAO() {
        return new DBEmpAddressDAOImpl();
    }

    @Override
    public EmployeeCategoryDAO getEmployeeCategoryDAO() {
        return new DBEmpCatDAOImpl();
    }

    @Override
    public EmployeeTypeDAO getEmployeeTypeDAO() {
        return new DBEmpTypeDAOImpl();
    }

    @Override
    public JobDAO getJobDAO() {
        return new DBJobDAOImpl();
    }

    @Override
    public ProjectDAO getProjectDAO() {
        return new DBProjectDAOImpl();
    }

    @Override
    public AttendanceDAO getAttendanceDAO() {
        return new DBAttendanceDAOImpl();
    }

    @Override
    public AuthDAO getAuthDAO() {
        return new DBAuthDAOImpl();
    }

    @Override
    public UserDAO getUserDAO() {
        return new DBUserDAOImpl();
    }

    @Override
    public UserRoleDAO getUserRoleDAO() {
        return new DBRoleDAOImpl();
    }

    @Override
    public SalaryDAO getSalaryDAO() {
        return new DBSalaryDAOImpl();
    }

    public static Connection getConnection() {
        return DB.getConnection();
    }
}
