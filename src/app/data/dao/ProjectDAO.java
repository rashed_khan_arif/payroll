package app.data.dao;

import app.model.Project;
import app.model.ProjectDetails;

import javax.swing.*;
import java.util.List;

/**
 * Created by arifk on 23.10.17.
 */
public interface ProjectDAO {
    int addProject(Project project);

    int updateProject(Project project);

    List<Project> getProjectList();

    Project getProject(int projectId);

    boolean deleteProject(int projectId);

    int assignProject(ProjectDetails projectDetails);

    List<ProjectDetails> getAssignedProjectMemberList(int projectId);
}
