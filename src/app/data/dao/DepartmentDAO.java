package app.data.dao;

import app.model.Department;

import java.util.List;

/**
 * Created by arifk on 25.8.17.
 */
public interface DepartmentDAO {

    int addDepartment(Department department);

    int updateDepartment(Department department);

    Department getDepartment(int deptId);

    List<Department> getDepartmentList();
}
