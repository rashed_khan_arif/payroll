package app.data.dao;

import app.model.Payroll;
import app.model.Salary;
import com.sun.istack.internal.Nullable;

import java.util.List;

/**
 * Created by arifk on 12.12.17.
 */
public interface SalaryDAO {
    List<Salary> getSalaryList();

    List<Salary> getSalaryList(int empId);

    Salary getSalaryByEmpId(int empId, int year);

    Salary getSalaryBySalaryId(int salId);

    Payroll getPayroll(int payrollId);

    Payroll getPayroll(int empId, int year, int month);

    List<Payroll> getPayrollList();

    List<Payroll> getPayrollList(@Nullable Integer empId, @Nullable Integer year, @Nullable Integer month);


    int insertPayroll(Payroll payroll);

    int insertSalary(Salary salary);

    int updateSalary(Salary salary);

    int updatePayroll(Payroll payroll);

}
