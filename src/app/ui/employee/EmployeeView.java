package app.ui.employee;

import app.config.ConfigKeys;
import app.core.ButtonColumn;
import app.core.DateSerializer;
import app.core.Role;
import app.model.Employee;
import app.modules.Common.Base;
import app.modules.Employee.EmployeeContract;
import app.modules.Employee.EmployeePresenter;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

/**
 * Created by arifk on 23.8.17.
 */
public class EmployeeView extends Base implements EmployeeContract.EmployeeView {
    private JPanel employeePanel;
    private JTable table1;
    private JTabbedPane tabbedPane1;
    private JPanel detailsPanel;
    private JLabel tvDesignation;
    private JLabel tvCellPhone;
    private JLabel ivProfileImage;
    private JLabel tvName;
    private JButton backButton;
    private JLabel tvPresentAddress;
    private JLabel tvPermanentAddress;
    private JLabel tvCity;
    private JLabel tvState;
    private JLabel tvZip;
    private JLabel tvEmail;
    private JLabel tvDob;
    private JLabel tvCountry;
    private EmployeePresenter presenter;
    private List<Employee> employeeList;
    private JPopupMenu popup;
    private int selectedEmployeeId;

    public EmployeeView() {
        setTitle("Employee");
        presenter = new EmployeePresenter(this);
        setUi();
        if (isDbConnected())
            init();

    }

    private void popupMenu(int empId) {
        popup = new JPopupMenu();
        JMenuItem viewProfile, editProfile;
        popup.add(viewProfile = new JMenuItem("View Profile", new ImageIcon("/res/ic.jpg")));
        viewProfile.setHorizontalTextPosition(JMenuItem.RIGHT);
        popup.add(editProfile = new JMenuItem("Edit Profile", new ImageIcon("2.gif")));
        editProfile.setHorizontalTextPosition(JMenuItem.RIGHT);
        popup.setBorder(new BevelBorder(BevelBorder.RAISED));
        popup.show(EmployeeView.this, MouseInfo.getPointerInfo().getLocation().x, MouseInfo.getPointerInfo().getLocation().y);
        viewProfile.addActionListener(e -> {
            viewProfile(empId);
        });
        editProfile.addActionListener(e -> {
            new AddEmployeeView(selectedEmployeeId);
            dispose();
        });

    }

    @Override
    public void setUi() {
        setUi(employeePanel);
    }

    @Override
    public void init() {
        presenter.getEmployeeData();
        if (currentUser.getRole().getRoleId() == Role.Employee.val) {
            viewProfile(currentUser.getEmpId());
            tabbedPane1.setSelectedIndex(1);
            backButton.setText("Edit Profile");
        }
        backButton.addActionListener(e -> {
                    if (currentUser.getRole().getRoleId() == Role.Employee.val) {
                        this.dispose();
                        new AddEmployeeView(currentUser.getEmpId());
                        return;
                    }
                    tabbedPane1.setSelectedIndex(0);
                }
        );
    }

    private Action edit = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int modelRow = Integer.valueOf(e.getActionCommand());
            for (int i = 0; i < employeeList.size(); i++) {

                if (i == modelRow) {
                    popupMenu(employeeList.get(i).getEmpId());
                    selectedEmployeeId = employeeList.get(i).getEmpId();
                    break;
                }
            }
        }

    };


    @Override
    public void setEmployeeData(List<Employee> employees) {
        employeeList = employees;
        String columnNames[] = {"ID", "Employee Name", "Email", "Cell Phone", "Job","Status" ,""};
        DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        for (Employee em : employeeList) {
            Object[] data = {String.valueOf(em.getEmpCode()),
                    em.getFullName(),
                    em.getEmail(),
                    String.valueOf(em.getCellPhone()),
                    em.getJob() != null ? em.getJob().getJobTitle() : null,
                    em.getActive()==1?"Active":"InActive",
                    " Action "
            };
            model.addRow(data);
        }

        table1.setModel(model);
        table1.setGridColor(Color.CYAN);
        table1.setRowMargin(2);
        ButtonColumn buttonColumn = new ButtonColumn(table1, edit, 6);
        buttonColumn.setMnemonic(KeyEvent.VK_D);
        table1.getColumnModel().getColumn(1).setMinWidth(150);

    }

    @Override
    public void setEmployeeDetails(Employee employeeDetails) {
        tabbedPane1.setSelectedIndex(1);
        if (employeeDetails.getFullName() != null)
            tvName.setText(employeeDetails.getFullName());
        if (employeeDetails.getCellPhone() != 0 || employeeDetails.getOfficePhone() != 0)
            tvCellPhone.setText(String.valueOf(employeeDetails.getCellPhone()).concat(" , ").concat(String.valueOf(employeeDetails.getOfficePhone())));
        if (employeeDetails.getJob() != null)
            tvDesignation.setText(employeeDetails.getJob().getJobTitle());
        tvPermanentAddress.setText(employeeDetails.getAddress() != null ? employeeDetails.getAddress().getAddress1() != null ? employeeDetails.getAddress().getAddress1() : "" : "");
        tvPresentAddress.setText(employeeDetails.getAddress() != null ? employeeDetails.getAddress().getAddress2() != null ? employeeDetails.getAddress().getAddress2() : "" : "");
        tvCity.setText(employeeDetails.getAddress() != null ? employeeDetails.getAddress().getCity() != null ? employeeDetails.getAddress().getCity() : "" : "");
        tvState.setText(employeeDetails.getAddress().getState() != null ? employeeDetails.getAddress().getState() != null ? employeeDetails.getAddress().getState() : "" : "");
        if (employeeDetails.getAddress().getZip() != 0)
            tvZip.setText(String.valueOf(employeeDetails.getAddress().getZip()));
        tvCountry.setText(presenter.getCountryName(employeeDetails.getAddress().getCountryId()));
        tvDob.setText(DateSerializer.getDateStringFromDate(employeeDetails.getDob()));
        tvEmail.setText(employeeDetails.getEmail() != null ? employeeDetails.getEmail() : "");
        if (employeeDetails.getImage() != null)
            ivProfileImage.setIcon(new ImageIcon(ConfigKeys.FILE_ROUTE + employeeDetails.getImage()));
    }

    private void viewProfile(int empId) {
        presenter.getEmployeeDetails(empId);
    }

}
