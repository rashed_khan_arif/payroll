package app.ui;

import app.modules.Common.Base;
import app.modules.Home.HomeContract;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.*;

/**
 * Created by arifk on 22.8.17.
 */
public class Home extends Base implements HomeContract.HomeView {
    private JButton addEmployeeButton;
    private JButton employeesButton;
    private JPanel panelHome;

    public Home() {
        setTitle("Home");
        setUi();
        init();
    }

    @Override
    public void setUi() {
        setUi(panelHome);
    }

    @Override
    public void init() {
       
    }



}
