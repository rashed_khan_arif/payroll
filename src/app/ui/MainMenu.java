package app.ui;

import app.core.Role;
import app.model.Project;
import app.model.Salary;
import app.modules.Common.Base;
import app.ui.admin.AdminView;
import app.ui.attendance.AttendanceView;
import app.ui.employee.AddEmployeeView;
import app.ui.employee.EmployeeView;
import app.ui.project.ProjectView;
import app.ui.salary.SalaryView;
import app.util.Menus;

import javax.swing.*;
import java.awt.*;

/**
 * Created by arifk on 18.8.17.
 */
public class MainMenu {
    JFrame context;

    public MainMenu() {

    }

    public JMenuBar getMenu(JFrame con) {
        context = con;
//        Font f = new Font("sans-serif", Font.PLAIN, 20);
//        UIManager.put("Menu.font", f);
        JMenu home, employee, salary, attendance, project, config;
        JMenuBar mb = new JMenuBar();
        home = new JMenu(Menus.Home.name());
        JMenuItem goHome = new JMenuItem("Home");
        home.add(goHome);
        JMenuItem salaryItem = new JMenuItem("Salary");

        JMenuItem setting = new JMenuItem("Setting");
        setting.addActionListener(e -> {
            context.dispose();
            new AdminView();
        });
        //Employee menu
        employee = getEmployeeMenu();
        salary = new JMenu(Menus.Salary.name());
        attendance = new JMenu(Menus.Attendance.name());
        project = new JMenu(Menus.Project.name());
        salary.add(salaryItem);
        config = new JMenu(Menus.Setting.name());
        config.add(setting);
        mb.setBackground(Color.BLUE);


        //generate menubar using menus
        if (Base.isAuth()) {
            mb.add(home);
            if (Base.currentUser.getRole().getRoleId() == Role.Employee.val || Base.currentUser.getRole().getRoleId() == Role.Admin.val) {
                mb.add(employee);
            }
            if (Base.currentUser.getRole().getRoleId() == Role.Admin.val) {
                mb.add(salary);
                mb.add(attendance);
                mb.add(project);
                mb.add(config);
            }
        }
        mb.add(Box.createRigidArea(new Dimension(100, 30)));
        mb.setBackground(Color.orange);

        JMenuItem projects = new JMenuItem("Projects");
        project.add(projects);

        //actions
        JMenuItem getAttendance = new JMenuItem("Attendance List");
        attendance.add(getAttendance);
        getAttendance.addActionListener(e -> {
            context.dispose();
            new AttendanceView();
        });

        goHome.addActionListener(e -> {
            context.dispose();
            new Home();
        });
        projects.addActionListener(e -> {
            context.dispose();
            new ProjectView();
        });
        salaryItem.addActionListener(e -> {
            context.dispose();
            new SalaryView();
        });

        return mb;
    }

    private JMenu getEmployeeMenu() {
        JMenu menu = new JMenu(Menus.Employee.name());
        menu.setBackground(Color.orange);

        JMenuItem manageEmployee = new JMenuItem("Manage Employee");
        if (Base.currentUser.getRole().getRoleId() == Role.Admin.val) {
            JMenuItem addEmployee = new JMenuItem("Add Employee");
            menu.add(addEmployee);
            addEmployee.addActionListener(e -> {
                context.dispose();
                new AddEmployeeView(null);
            });
        }
        menu.add(manageEmployee);


        manageEmployee.addActionListener(e -> {
            context.dispose();
            new EmployeeView();
        });
        return menu;
    }


}
