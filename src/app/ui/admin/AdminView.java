package app.ui.admin;

import app.model.*;
import app.modules.Setting.SettingContract;
import app.modules.Setting.SettingPresenter;
import app.modules.Common.Base;
import com.sun.istack.internal.Nullable;
import sun.security.provider.MD5;

import javax.swing.*;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;
import java.util.Map;

/**
 * Created by arifk on 20.9.17.
 */
public class AdminView extends Base implements SettingContract.SettingView {

    private JButton addDepartmentButton;
    private JButton addCategoryButton;
    private JButton addJobButton;
    private JButton addTypeButton;
    private JPanel adminPanel;
    private JTabbedPane tabbedPane1;
    private JPanel deptTab;
    private JPanel catTab;
    private JPanel typsTab;
    private JPanel jobsTab;
    private JTable deptTable;
    private JTable catTable;
    private JTable jobTable;
    private JTable typeTable;
    private JButton departmentListButton;
    private JButton categoriesButton;
    private JButton jobsButton;
    private JButton typesButton;
    private JButton addNewUserButton;
    private JButton userListButton;
    private JTable userTable;
    private JScrollPane userPane;
    private JComboBox cmbParentDepartment;
    private JComboBox cmbManger;
    private JTextField textDepartmentName;
    private JButton saveDepartmentButton;
    private JTextField textLocation;
    private JTextField textCatName;
    private JTextField textDesc;
    private JButton saveCategoryButton;
    private JTextField textJobTitle;
    private JComboBox cmbDepartment;
    private JTextField textJobDesc;
    private JButton saveJobButton;
    private JTextField txtTypeName;
    private JTextField textTypeDesc;
    private JButton saveTypeButton;
    private JButton saveUserButton;
    private JComboBox cmbEmplRole;
    private JPasswordField textPassword;
    private JPasswordField textConfirmPassword;
    private JTextField textEmpId;
    private JCheckBox enpActiveCheckBox;
    private SettingPresenter presenter;

    public AdminView() {
        presenter = new SettingPresenter(this);
        setUi();
        init();

    }

    @Override
    public void setUi() {
        setTitle("Setting");
        setUi(adminPanel);

    }

    @Override
    public void init() {
        presenter.getDepartments();
        presenter.getEmployeeCategories();
        presenter.getEmployeeTypes();
        presenter.getJobs();
        presenter.getUserList();
        presenter.getUserRoles();
        presenter.getEmployeeList();

        departmentListButton.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(index.department.i);
            presenter.getDepartments();
        });
        categoriesButton.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(index.category.i);
            presenter.getEmployeeCategories();
        });
        jobsButton.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(index.job.i);
            presenter.getJobs();
        });
        typesButton.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(index.type.i);
            presenter.getEmployeeTypes();
        });
        userListButton.addActionListener(e -> {
            tabbedPane1.setSelectedIndex(index.users.i);
            presenter.getUserList();
        });
        addDepartmentButton.addActionListener(e -> tabbedPane1.setSelectedIndex(index.addDepartment.i));
        addCategoryButton.addActionListener(e -> tabbedPane1.setSelectedIndex(index.addCat.i));
        addJobButton.addActionListener(e -> tabbedPane1.setSelectedIndex(index.addJob.i));
        addTypeButton.addActionListener(e -> tabbedPane1.setSelectedIndex(index.addType.i));
        addNewUserButton.addActionListener(e -> tabbedPane1.setSelectedIndex(index.addNewUser.i));
        saveUserButton.addActionListener(e -> createUser());
        saveTypeButton.addActionListener(e -> createEmployeeType());
        saveDepartmentButton.addActionListener(e -> createDepartment());
        saveCategoryButton.addActionListener(e -> createCategory());
        saveJobButton.addActionListener(e -> createJob());
    }

    private void createJob() {
        Job job = new Job();
        job.setJobTitle(textJobTitle.getText());
        try {
            job.setDeptId(((Department) cmbDepartment.getSelectedItem()).getDeptId());
        } catch (ClassCastException e) {
            message("Please select department !");
            return;
        }
        job.setJobDesc(textJobDesc.getText());
        int jobId = presenter.createJob(job);
        if (jobId != 0) {
            message("Job added ");
        } else {
            message("Failed to add job !");
        }
    }

    private void createCategory() {
        EmployeeCategory category = new EmployeeCategory();
        category.setCatName(textCatName.getText());
        category.setCatDesc(textDesc.getText());
        int catId = presenter.createCategory(category);
        if (catId != 0) {
            message("Employee Category added");
        } else
            message("Failed to add category !");
    }

    private void createDepartment() {
        Department dept = new Department();
        try {
            dept.setDeptParentId(((Department) (cmbParentDepartment.getSelectedItem())).getDeptId());
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        try {
            dept.setManagerId(((Employee) (cmbManger.getSelectedItem())).getEmpId());
        } catch (ClassCastException e) {
            e.printStackTrace();
        }


        dept.setDeptName(textDepartmentName.getText());
        dept.setDeptDesc(textDesc.getText());
        dept.setLocation(textLocation.getText());
        int deptId = presenter.createDepartment(dept);
        if (deptId != 0) {
            message("Department Added !");
        } else
            message("Failed to add department");
    }

    private void createEmployeeType() {
        EmployeeType type = new EmployeeType();
        type.setName(txtTypeName.getText());
        type.setNote(textTypeDesc.getText());
        int typeId = presenter.createType(type);
        if (typeId != 0) {
            message("Employee Type added");
        } else
            message("Failed ");
    }

    private void createUser() {
        if (textEmpId.getText().isEmpty()) {
            message("Employee Id required !");
            return;
        }
        if (textPassword.getPassword().toString().isEmpty()) {
            message("Password required !");
            return;
        }
        if (!String.valueOf(textPassword.getPassword()).equals(String.valueOf(textConfirmPassword.getPassword()))) {
            message("Password Doesn't match ! ");
            return;
        }
        User user = new User();
        Employee employee = presenter.checkEmpId(textEmpId.getText());
        if (employee == null) {
            message("Employee Not found");
            return;
        }
        if (presenter.checkUserExits(employee.getEmpId())) {
            message("User already exits");
            return;
        }
        user.setEmpId(employee.getEmpId());
        user.setActive(enpActiveCheckBox.isSelected() ? 1 : 0);
        user.setPassword(String.valueOf(textPassword.getPassword()));
      try{
          UserRole role = (UserRole) cmbEmplRole.getSelectedItem();
          user.setRoleId(role.getRoleId());
      }catch (ClassCastException e){
          message("Please select role !");
          return;
      }
        boolean success = presenter.createUser(user);
        if (success) {
            message("User Created !");
        } else {
            message("Failed ! ");
        }
    }

    private enum index {
        department(0), category(1), job(2), type(3), users(4), addDepartment(5), addCat(6), addJob(7), addType(8), addNewUser(9);
        int i;

        index(int i) {
            this.i = i;
        }
    }

    @Override
    public void setUserList(List<User> users) {
        String columnNames[] = {"User Name", "Role", "Active"};
        DefaultTableModel model = new DefaultTableModel(columnNames, 0);

        for (User user : users) {
            Object[] data = {
                    user.getEmployee().getFullName(), user.getRole().getRoleName(), user.isActive() ? "Active" : "Inactive"};
            model.addRow(data);
        }
        userTable.setModel(model);
        userTable.setGridColor(Color.CYAN);
        userTable.setRowMargin(2);
    }

    @Override
    public void setTypes(List<EmployeeType> types) {
        String columnNames[] = {"Type Name", "Description"};
        DefaultTableModel model = new DefaultTableModel(columnNames, 0);

        for (EmployeeType type : types) {
            Object[] data = {
                    type.getName(), type.getNote()};
            model.addRow(data);
        }
        typeTable.setModel(model);
        typeTable.setGridColor(Color.CYAN);
        typeTable.setRowMargin(2);
    }

    @Override
    public void setJobsData(List<Job> jobs) {
        String columnNames[] = {"Job Title", "Department", "Description"};
        DefaultTableModel model = new DefaultTableModel(columnNames, 0);

        for (Job job : jobs) {
            Object[] data = {
                    job.getJobTitle(),
                    job.getDepartment().getDeptName(),
                    job.getJobDesc()};
            model.addRow(data);
        }
        jobTable.setModel(model);
        jobTable.setRowMargin(3);
        jobTable.setGridColor(Color.CYAN);
        jobTable.setRowMargin(2);
    }

    @Override
    public void setDepartmentDataToView(List<Department> departments) {
        String columnNames[] = {"Department Name", "Manager", "Parent Department", "Location", "Description"};
        DefaultTableModel model = new DefaultTableModel(columnNames, 0);

        for (Department dp : departments) {
            Object[] data = {String.valueOf(dp.getDeptName()),
                    dp.getManager() == null ? "" : dp.getManager().getFullName(),
                    dp.getParentDept() == null ? "" : dp.getParentDept().getDeptName(),
                    dp.getLocation(),
                    dp.getDeptDesc()};
            model.addRow(data);
        }
        deptTable.setModel(model);
        deptTable.setGridColor(Color.CYAN);
        deptTable.setRowMargin(3);
        cmbDepartment.removeAllItems();
        cmbDepartment.addItem("-- select department --");
        departments.forEach(department -> {
            cmbDepartment.addItem(department);
        });
        cmbParentDepartment.removeAllItems();
        cmbParentDepartment.addItem("-- select department --");
        departments.forEach(department -> {
            cmbParentDepartment.addItem(department);
        });
    }

    @Override
    public void setCategoriesData(List<EmployeeCategory> categories) {
        String columnNames[] = {"Category", "Description"};
        DefaultTableModel model = new DefaultTableModel(columnNames, 0);

        for (EmployeeCategory cat : categories) {
            Object[] data = {String.valueOf(cat.getCatName()),
                    cat.getCatDesc()};
            model.addRow(data);
        }
        catTable.setModel(model);
        catTable.setRowMargin(3);
        catTable.setGridColor(Color.CYAN);
        catTable.setRowMargin(2);
    }

    @Override
    public void setUserRolesToView(List<UserRole> roleList) {
        cmbEmplRole.addItem("-- select role --");
        roleList.forEach(role -> {
            cmbEmplRole.addItem(role);
        });
    }

    @Override
    public void setEmployeeListToCombo(List<Employee> employeeList) {
        cmbManger.addItem("-- select manager --");
        employeeList.forEach(employee -> {
            cmbManger.addItem(employee);
        });
    }
}
