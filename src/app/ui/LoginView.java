package app.ui;

import app.model.LoginData;
import app.model.User;
import app.modules.Auth.AuthContract;
import app.modules.Auth.AuthPresenter;
import app.modules.Common.Base;
import app.ui.employee.AddEmployeeView;
import com.sun.istack.internal.Nullable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by arifk on 21.8.17.
 */
public class LoginView extends Base implements AuthContract.AuthView {
    private JPanel loginPanel;
    private JTextField textUserId;
    private JTextField textPassword;
    private JButton loginButton;
    private JButton registrationButton;
    private AuthContract.AuthPresenter presenter;

    public LoginView() {
        setTitle("LoginView");
        presenter = new AuthPresenter(this);
        setUi();
        init();
    }

    @Override
    public void setUi() {
        setLocationRelativeTo(null);
        //  setJMenuBar(new MainMenu().getMenu());
        setContentPane(loginPanel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        try {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            setSize(screenSize.width, screenSize.height);
            setExtendedState(Frame.MAXIMIZED_BOTH);
            setVisible(true);
        } catch (Exception e) {
            message(e.getMessage());
        }

    }

    @Override
    public void init() {
        textUserId.addActionListener(e -> textPassword.requestFocusInWindow());
        textPassword.addActionListener(e -> checkLogin());
        loginButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (!isDbConnected()) {
                    message("Database connection failed !");
                    return;
                }
                checkLogin();
            }
        });
        registrationButton.addActionListener(e -> {
            this.dispose();
            new AddEmployeeView(null);
        });
    }

    private void checkLogin() {
        LoginData data = new LoginData();
        data.setUserId(textUserId.getText());
        data.setPassWord(textPassword.getText());
        presenter.checkLoginInfo(data);
    }

    @Override
    public void isAuth(boolean auth, @Nullable User user) {
        if (auth) {
            //  insertUserToSession(user);
            currentUser = user;
            new Home();
            dispose();
        } else {
            message("Failed  incorrect information");
        }
    }
}

